﻿Public Class Form1
    Dim numRandom As Integer
    Dim contador As Integer = 0
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles generarNum.Click
        numRandom = CInt(Math.Ceiling(Rnd() * 100)) + 1
        generarNum.Enabled = False

    End Sub

    Private Sub introdNum_TextChanged(sender As Object, e As EventArgs) Handles introdNum.TextChanged

    End Sub

    Private Sub limpiar_Click(sender As Object, e As EventArgs) Handles limpiar.Click
        generarNum.Enabled = True
        numRandom = vbNull
        cajaNumeros.Items.Clear()

    End Sub

    Private Sub salir_Click(sender As Object, e As EventArgs) Handles salir.Click
        Close()

    End Sub

    Private Sub cajaNumeros_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cajaNumeros.SelectedIndexChanged

    End Sub

    Private Sub introdNum_KeyPress(sender As Object, e As KeyPressEventArgs) Handles introdNum.KeyPress

        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            Dim numeroIntroducido = introdNum.Text
            If numeroIntroducido < numRandom Then
                cajaNumeros.Items.Add("El número " + numeroIntroducido + " es demasiado pequeño")
                contador += 1
            ElseIf numeroIntroducido > numRandom Then
                cajaNumeros.Items.Add("El número " + numeroIntroducido + " es demasiado grande")
                contador += 1
            Else
                cajaNumeros.Items.Add("¡FELICIDADES! El número " + numeroIntroducido + " coincide con el número aleatorio")
                cajaNumeros.Items.Add("Intentos realizados " + contador.ToString)
            End If

            MsgBox(contador)
            MsgBox(numRandom)
        End If

    End Sub

End Class
