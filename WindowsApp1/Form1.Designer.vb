﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.generarNum = New System.Windows.Forms.Button()
        Me.introdNum = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.limpiar = New System.Windows.Forms.Button()
        Me.salir = New System.Windows.Forms.Button()
        Me.cajaNumeros = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'generarNum
        '
        Me.generarNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.generarNum.Location = New System.Drawing.Point(59, 25)
        Me.generarNum.Name = "generarNum"
        Me.generarNum.Size = New System.Drawing.Size(274, 33)
        Me.generarNum.TabIndex = 0
        Me.generarNum.Text = "Obtener un número entre 1 y 100"
        Me.generarNum.UseVisualStyleBackColor = True
        '
        'introdNum
        '
        Me.introdNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.25!)
        Me.introdNum.Location = New System.Drawing.Point(191, 72)
        Me.introdNum.Name = "introdNum"
        Me.introdNum.Size = New System.Drawing.Size(66, 23)
        Me.introdNum.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(66, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Teclea un número:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(66, 124)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(122, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Número tecleados:"
        '
        'limpiar
        '
        Me.limpiar.Location = New System.Drawing.Point(110, 279)
        Me.limpiar.Name = "limpiar"
        Me.limpiar.Size = New System.Drawing.Size(75, 23)
        Me.limpiar.TabIndex = 5
        Me.limpiar.Text = "Limpiar"
        Me.limpiar.UseVisualStyleBackColor = True
        '
        'salir
        '
        Me.salir.Location = New System.Drawing.Point(200, 279)
        Me.salir.Name = "salir"
        Me.salir.Size = New System.Drawing.Size(75, 23)
        Me.salir.TabIndex = 6
        Me.salir.Text = "Salir"
        Me.salir.UseVisualStyleBackColor = True
        '
        'cajaNumeros
        '
        Me.cajaNumeros.FormattingEnabled = True
        Me.cajaNumeros.Location = New System.Drawing.Point(43, 143)
        Me.cajaNumeros.Name = "cajaNumeros"
        Me.cajaNumeros.Size = New System.Drawing.Size(308, 108)
        Me.cajaNumeros.TabIndex = 9
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(413, 327)
        Me.Controls.Add(Me.cajaNumeros)
        Me.Controls.Add(Me.salir)
        Me.Controls.Add(Me.limpiar)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.introdNum)
        Me.Controls.Add(Me.generarNum)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents generarNum As Button
    Friend WithEvents introdNum As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents limpiar As Button
    Friend WithEvents salir As Button
    Friend WithEvents cajaNumeros As ListBox
End Class
